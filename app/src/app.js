/**
 * Created by rubenoliveira on 03-07-2016.
 * http://stackoverflow.com/questions/40485/optimizing-conways-game-of-life
 * http://www.quesucede.com/public/gameoflife/index.html
 */
(function () {
    var GRID_SIZE = {
        col: 20,
        lin: 20
    };

    var grid = [];
    var intervalId;

    function createLine (l) {
        var line = [];

        for(var i=0; i<GRID_SIZE.col; i++) {
            var cell = new GOL.CELL(l, i);
            line.push(cell);
        };

        return line;
    }

    function getLTCell(col, lin) {
        return (grid[lin - 1] && grid[lin-1][col-1]) ? grid[lin-1][col-1] : null;
    }

    function getLcell(col, lin) {
        return (grid[lin][col-1]) ? grid[lin][col-1] : null;
    }

    function getLBCell(col, lin) {
        return (grid[lin+1] && grid[lin+1][col-1]) ? grid[lin+1][col-1] : null;
    }

    function getBCell(col, lin) {
        return (grid[lin + 1]) ? grid[lin + 1][col] : null;
    }

    function getRBCell(col, lin) {
        return (grid[lin + 1] && grid[lin + 1][col + 1]) ? grid[lin + 1][col + 1] : null;
    }

    function getRCell(col, lin) {
        return (grid[lin] && grid[lin][col + 1]) ? grid[lin][col + 1] : null;
    }

    function getRTCell(col, lin) {
        return (grid[lin - 1] && grid[lin -1][col + 1]) ? grid[lin -1][col + 1] : null;
    }

    function getTCell(col, lin) {
        return (grid[lin - 1] && grid[lin -1][col]) ? grid[lin -1][col] : null;
    }

    function referenceCells() {
        for(var l=0; l<grid.length; l++) {
            var line = grid[l];

            for(var c=0; c<line.length; c++) {
                var cell = line[c];

                cell.setNeighbours([
                    getLTCell(c, l),
                    getLcell(c, l),
                    getLBCell(c, l),
                    getBCell(c, l),
                    getRBCell(c, l),
                    getRCell(c, l),
                    getRTCell(c, l),
                    getTCell(c, l)
                ].filter(function(elem) {
                        return elem != null ? true : false;
                    }));
            }
        }
    }

    function createGrid() {
        var line;

        for (var i = 0; i < GRID_SIZE.lin; i++) {
            line = createLine(i);
            var domLine = document.createElement('div');
            domLine.setAttribute('class', 'line');


            for(var j=0; j<line.length; j++) {
                domLine.appendChild(line[j].getDomElement());
            }

            grid.push(line);

            window.document.body.appendChild(domLine);
        }
    }

    window.GOL.app = {
        start: function () {
            createGrid();
            referenceCells();
        },
        tick: function() {
            for (var i = 0; i < grid.length; i++) {
                var line = grid[i];

                for(var j=0; j<line.length; j++) {
                    line[j].goNextGen();
                }
            }

            for (var i = 0; i < grid.length; i++) {
                var line = grid[i];

                for(var j=0; j<line.length; j++) {
                    line[j].updateNextGen();
                }
            }

        },
        play: function() {
            intervalId = window.setInterval(window.GOL.app.tick, 500);
        },
        grid: grid
    }
})(window);
