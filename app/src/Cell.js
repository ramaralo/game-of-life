/**
 * Created by rubenoliveira on 03-07-2016.
 */
GOL.CELL = function (line, column) {
    var self = this;
    var neighbourCells = [];

    var nextState = null;

    this.state = 0;
    this.line = line;
    this.column = column;
    this.neighbours = [];

    var domElement;

    function changeNextState(newState) {
        nextState = newState;
    }

    function countAliveNeighbours() {
        var alive = 0;

        for(var i=0; i<neighbourCells.length; i++) {
            if(neighbourCells[i] !== null) {
                alive += neighbourCells[i].state;
            }
        }

        return alive;
    }

    function toogleSatte() {
        if(self.state === 0) {
            self.state = 1;
        }
        else {
            self.state = 0;
        }
    }

    function updateDom() {
        if (self.state === 1) {
            domElement.setAttribute('class', 'cell alive')
        }
        else {
            domElement.setAttribute('class', 'cell dead')
        }

        /*domElement.innerHTML = self.state + '/' + nextState;*/
    }

    function moveNextStateToCurrent() {
        if(nextState !== null) {

            self.state = nextState;
            nextState = null;
        }
    }

    function updateNextState () {
        var neighboursAlive = countAliveNeighbours();

        if(neighboursAlive > 3) {
            changeNextState(0);
        }

        if(neighboursAlive === 3) {
            changeNextState(1);
        }

        if(neighboursAlive === 2) {
            changeNextState(self.state);
        }

        if (neighboursAlive < 2) {
            changeNextState(0);
        }
    };

    this.goNextGen = function() {
        moveNextStateToCurrent();
        updateDom();
    };

    this.updateNextGen = function() {
        updateNextState();
    };

    this.click = function() {
        toogleSatte();
        updateDom();
    }

    this.setNeighbours = function (neightbourList) {
        neighbourCells = neightbourList;

        this.neighbours = neightbourList;
    };

    this.getDomElement = function () {
        return domElement;
    };

    (function () {
        domElement = window.document.createElement('div');
        domElement.setAttribute('class', 'cell dead');
        domElement.addEventListener('click', function() {
            toogleSatte();
            updateDom();
        })
    })();
};